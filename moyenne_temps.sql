\timing off
\set echo all

CREATE TEMP TABLE donnees_brutes
(pid        int,
test_num   int,
titre       text,
pgbench_size    int,
volume_archive_wals bigint,
nb_archives_restaurees int,
date_debut  timestamptz,
date_fin  timestamptz,
archive_compress_bin    text,
duree_s int
);


-- PARAM_SOURCE.csv à remplacer à coup de sed ou à symlinker
-- because \copy n'accepte pas de :'variable' !
\COPY donnees_brutes FROM PARAM_SOURCE.csv with (format csv,header,delimiter ';') ;

--select * from donnees_brutes;

\pset format csv
\pset csv_fieldsep ;
\o :sortie

SELECT 
  pid,count(test_num) nb_tests,titre, archive_compress_bin,pgbench_size,
  round(avg(volume_archive_wals),1) volume_archive_wals,
  round(avg(nb_archives_restaurees),1) nb_archives_restaurees, 
  round(avg(duree_s),1) duree_s
  FROM donnees_brutes
  GROUP BY 1,3,4,5
  ORDER BY duree_s
;
