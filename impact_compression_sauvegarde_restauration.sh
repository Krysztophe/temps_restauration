#!/usr/bin/env bash

CLN=testresto

# TODO wal_segsize !
#      virer le cache de l'OS avant restau ?

NBRUNS=${NBRUNS:-3}  #Nb de tests pour reproduire

export V=${V:-12}
export PGPORT=${PGPORT:-12090}
export PGDATABASE=postgres
export PGUSER=postgres
export PGOPTIONS='-c synchronous_commit=off'
unset PAGER

export PITRERY_PATH=${PITRERY_PATH:-'/opt/pitrery'}          # pitrery 3
export PITRERY_CONF=/tmp/pitrery.$CLN.conf
export BACKUP_DIR=${BACKUP_DIR:-"/PGDATA2/PITR/$CLN"}
export ARCHIVE_DIR="$BACKUP_DIR"/wals

export LANG=C

PGBENCH_SIZE=${PGBENCH_SIZE:-100}

# données brutes
RES_CSV=temps_resto.$HOSTNAME.$PGBENCH_SIZE.$(date  +%Y%m%d_%H%M).csv
# moyennées
RES_CSV_MOY=temps_resto.$HOSTNAME.$PGBENCH_SIZE.$(date  +%Y%m%d_%H%M)-moy.csv


function log_csv() {

if [ ${NB_ARCHIVES_RESTAUREES:-0} -le 0 ] ; then
    echo "PID;test_num;titre;pgbench_size;volume_archive_wals;nb_archives_restaurees;date_debut;date_fin;archive_compress_bin;Durée (s)" >> $RES_CSV
else
    echo "$$;$NUM_TEST;$TITRE;$PGBENCH_SIZE;$VOL_ARCHIVE_WALS;$NB_ARCHIVES_RESTAUREES;$DATE_DEBUT;$DATE_FIN;$ARCHIVE_COMPRESS_BIN;$DUREE" >> $RES_CSV
fi
}

echo Cleaning

set -x

sudo pg_dropcluster $V $CLN --stop 2>/dev/null
#sudo -iu postgres $PITRERY_PATH/pitrery -f "$PITRERY_CONF" purge 2>/dev/null
sudo -iu postgres rm -rf "$BACKUP_DIR" 2>/dev/null

set -xue

log_csv # entêtes

# Liste des tests

declare -A titre
declare -A comp
declare -A decomp
declare -A walcmp
declare -A fsync


#  xz 
tst="xz_6"
titre["$tst"]="$tst"
comp["$tst"]="xz"   # défaut est 6
decomp["$tst"]="unxz"
walcmp["$tst"]="off"
fsync["$tst"]="on"

tst="xz_0"
titre["$tst"]="$tst"
comp["$tst"]="xz -0"
decomp["$tst"]="unxz"
walcmp["$tst"]="off"
fsync["$tst"]="on"

tst="xz_9"  # -8 devrait suffire pour 16 Mo
titre["$tst"]="$tst"
comp["$tst"]="xz -9"
decomp["$tst"]="unxz"
walcmp["$tst"]="off"
fsync["$tst"]="on"

# gzip, et pigz

tst="gzip_1"
titre["$tst"]=$tst
comp["$tst"]="gzip -f -1"
decomp["$tst"]="gunzip"
walcmp["$tst"]="off"
fsync["$tst"]="on"
 
tst="gzip_4"
titre["$tst"]=$tst
comp["$tst"]="gzip -f -4"
decomp["$tst"]="gunzip"
walcmp["$tst"]="off"
fsync["$tst"]="on"
 
tst="gzip_1_pigz4"
titre["$tst"]=$tst
comp["$tst"]="pigz -p4 -f -1"
decomp["$tst"]="pigz -p4 -d"   # parallélisation de la décompresssion pas vraiment prévue avec pigz
walcmp["$tst"]="off"
fsync["$tst"]="on"
 
tst="gzip_9_pigz4"
titre["$tst"]=$tst
comp["$tst"]="pigz -p4 -f -9"
decomp["$tst"]="pigz -p4 -d"   # parallélisation de la décompresssion pas vraiment prévue avec pigz
walcmp["$tst"]="off"
fsync["$tst"]="on"
 
tst="gzip_9"
titre["$tst"]="$tst"
comp["$tst"]="gzip -f -9"
decomp["$tst"]="gunzip"
walcmp["$tst"]="off"
fsync["$tst"]="on"

# bz2

tst="bzip2_1"
titre["$tst"]="$tst"
comp["$tst"]="bzip2 -1"
decomp["$tst"]="bzip2 -d"
walcmp["$tst"]="off"
fsync["$tst"]="on"
 
tst="bzip2_4"
titre["$tst"]="$tst"
comp["$tst"]="bzip2 -4"
decomp["$tst"]="bzip2 -d"
walcmp["$tst"]="off"
fsync["$tst"]="on"
 
tst="bzip2_9"
titre["$tst"]="$tst"
comp["$tst"]="bzip2 -9"
decomp["$tst"]="bzip2 -d"
walcmp["$tst"]="off"
fsync["$tst"]="on"
 
tst="pbzip2_9_j3"
titre["$tst"]="$tst"
comp["$tst"]="pbzip2 -9 -j3"
decomp["$tst"]="pbzip2 -d -j3"
walcmp["$tst"]="off"
fsync["$tst"]="on"

tst="pbzip2_1_j3"
titre["$tst"]="$tst"
comp["$tst"]="pbzip2 -1 -j3"
decomp["$tst"]="pbzip2 -d -j3"
walcmp["$tst"]="off"
fsync["$tst"]="on"

# pas de compression des archives

tst="pas_de_compression"
titre["$tst"]="$tst"
comp["$tst"]=""
decomp["$tst"]=""
walcmp["$tst"]="off"
fsync["$tst"]="on"
 
tst="pas_de_compression_wal_compression_on"
titre["$tst"]="$tst"
comp["$tst"]=""
decomp["$tst"]=""
walcmp["$tst"]="on"
fsync["$tst"]="on"

tst="pas_de_compression_fsync_off"
titre["$tst"]="$tst"
comp["$tst"]=""
decomp["$tst"]=""
walcmp["$tst"]="off"
fsync["$tst"]="off"

# winners possibles

tst="gzip_1_wal_compression_fsync_off"
titre["$tst"]="$tst"
comp["$tst"]="gzip -f -1"
decomp["$tst"]="gunzip"
walcmp["$tst"]="on"
fsync["$tst"]="off"


tst="gzip_4_wal_compression_fsync_off"
titre["$tst"]="$tst"
comp["$tst"]="gzip -f -4"
decomp["$tst"]="gunzip"
walcmp["$tst"]="on"
fsync["$tst"]="off"

tst="pbzip2_1_j4_wal_compression_fsync_off"
titre["$tst"]="$tst"
comp["$tst"]="pbzip2 -1 -j4"
decomp["$tst"]="pbzip2 -d -j4"
walcmp["$tst"]="on"
fsync["$tst"]="off"


tst="pbzip2_4_j4_wal_compression_fsync_off"
titre["$tst"]="$tst"
comp["$tst"]="pbzip2 -4 -j4"
decomp["$tst"]="pbzip2 -d -j4"
walcmp["$tst"]="on"
fsync["$tst"]="off"


tst="xz_0_wal_compression_fync_off"
titre["$tst"]="$tst"
comp["$tst"]="xz -0"
decomp["$tst"]="unxz"   
walcmp["$tst"]="on"
fsync["$tst"]="off"

tst="xz_0_ignchk_wal_compression_fsync_off"  # avec sécurité off
titre["$tst"]="$tst"
comp["$tst"]="xz -0"
decomp["$tst"]="unxz  --ignore-check"    # ignore_check est dangereux pour ce que ça rapporte...
walcmp["$tst"]="on"
fsync["$tst"]="off"



echo debug

for t in ${titre[@]} ; do
    echo "$t : ${titre[$t]} :  ${comp[$t]} : ${decomp[$t]} "
done

echo 
echo Beginning tests...
echo 
NUM_TEST=0
NB_TOTAL_TESTS=$(( $NBRUNS * ${#titre[@]} ))

for run in $(seq 1 $NBRUNS) ; do

for t in ${titre[@]} ; do

    TITRE="$t"
    NUM_TEST=$(( $NUM_TEST + 1 ))

    echo 
    echo Test $NUM_TEST / $NB_TOTAL_TESTS - Test : $t   ${titre["$t"]} - Run $run 
    echo

    echo "Recréation d'une instance fraîche, config par défaut"

    sudo -iu postgres rm -rf "$BACKUP_DIR"
    sudo -iu postgres mkdir -p "$BACKUP_DIR"


    sudo pg_createcluster $V $CLN --createclusterconf=/dev/null  --port=$PGPORT --start-conf=manual -o shared_buffers='1GB' -o fsync="${fsync["$t"]}"  -o archive_mode=on -o archive_command="$PITRERY_PATH/archive_wal -C $PITRERY_CONF %p" -o wal_compression="${walcmp["$t"]}" -o log_line_prefix='%t [%p]: [%l-1] user=%u,db=%d,app=%a,client=%h ' -- --auth-local=trust --auth-host=reject --data-checksums  --lc-messages='C'

    pg_lsclusters|grep "$CLN"

    PGDATA=$(echo $(pg_conftool $V $CLN -s show data_directory ))
    PGWAL="$PGDATA"/pg_wal
    PGLOG=$(echo $(pg_lsclusters |grep -E "^$V(.*)$CLN"|awk '{print $7}'))
    echo "Chemins : $PGDATA    $PGLOG"

    echo
    echo "Pitrery config"
    echo

    ARCHIVE_COMPRESS_BIN=${comp["$t"]}
    ARCHIVE_UNCOMPRESS_BIN=${decomp["$t"]}
    if [ "${ARCHIVE_COMPRESS_BIN:-NO}" = "NO" ] ; then
        boocomp=no
        suffix=
    else
        boocomp=yes
        # file extension
        if [ "${ARCHIVE_COMPRESS_BIN:0:4}" = 'gzip' -o "${ARCHIVE_COMPRESS_BIN:0:4}" = 'pigz' ] ; then 
            suffix='gz'
        elif [ "${ARCHIVE_COMPRESS_BIN:0:6}" = 'pbzip2' -o "${ARCHIVE_COMPRESS_BIN:0:5}" = 'bzip2' ] ; then 
            suffix='bz2'
        elif [ "${ARCHIVE_COMPRESS_BIN:0:2}" = 'xz' ] ; then 
            suffix='xz'
        else
            echo ; echo "Suffixe introuvable pour ${ARCHIVE_COMPRESS_BIN} !" ; exit 11
        fi
    fi
        
    
echo "PGDATA=$PGDATA
PGPORT=$PGPORT
PGHOST=/var/run/postgresql
BACKUP_DIR=$BACKUP_DIR
STORAGE=rsync
RESTORE_COMMAND='"$PITRERY_PATH"/restore_wal -C "$PITRERY_CONF" %f %p'
ARCHIVE_DIR=$ARCHIVE_DIR
PURGE_KEEP_COUNT=0
BACKUP_COMPRESS_BIN='gzip -f -4'  
BACKUP_UNCOMPRESS_BIN='gunzip'
BACKUP_COMPRESS_SUFFIX='gz'
ARCHIVE_COMPRESS="$boocomp"
ARCHIVE_COMPRESS_BIN='"$ARCHIVE_COMPRESS_BIN"'
ARCHIVE_UNCOMPRESS_BIN='"$ARCHIVE_UNCOMPRESS_BIN"'
ARCHIVE_COMPRESS_SUFFIX=$suffix
" |tee $PITRERY_CONF

    sudo pg_ctlcluster $V $CLN start

    psql -Xc 'show shared_buffers' -c 'show archive_command' -c 'show archive_mode' 

    echo "1er Pgbench"

    pgbench -i  -s $PGBENCH_SIZE 2>/dev/null
    psql -Xc 'SELECT * FROM pg_stat_archiver'

    echo "Backup full"
    psql -Xc 'checkpoint'
    time sudo -iu postgres $PITRERY_PATH/pitrery -f "$PITRERY_CONF" backup

    echo "2è Pgbench"

    pgbench -i  -s $PGBENCH_SIZE

    echo
    echo "Attente fin archivage"
    echo

    n=1
    while [ $n -gt 0 ]; do
    n=$(sudo find "$PGWAL"/archive_status -name '*.ready' -printf '.'|wc -c)
    echo "Archivage: $n fichiers .ready..."
    psql -Xxc 'SELECT * FROM pg_stat_archiver'
    sleep 1
    #if [ $n -gt 10000 ] ; then  echo "TROP ATTENDU" ; exit 10 ; fi
    done

    echo
    echo "Volumétrie archivée"
    echo

    sudo -iu postgres $PITRERY_PATH/pitrery -f "$PITRERY_CONF" list
    sudo -iu postgres $PITRERY_PATH/pitrery -f "$PITRERY_CONF" check -B -A
    sudo du -smxh $BACKUP_DIR/
    sudo du -smxh $ARCHIVE_DIR/
    VOL_ARCHIVE_WALS=$(echo $(sudo du -sx "$ARCHIVE_DIR" | awk '{print $1}'))

    echo
    echo Restauration
    echo

    sudo pg_ctlcluster $V $CLN stop --force --mode immediate
    sudo rm -rf "$PGDATA"
    sudo -iu postgres $PITRERY_PATH/pitrery -f "$PITRERY_CONF" restore -D "$PGDATA" -R -T

    echo "Configuration de redémarrage"
    # NB : A cause de pitrery 3 en v12 sur Debian https://github.com/dalibo/pitrery/issues/113
    # il pourrait faire ça tout seul dans le futur...
    sudo -iu postgres bash -c "grep restore_command $PGDATA/restored_config_files/postgresql.conf >> $PGDATA/postgresql.auto.conf"
    sudo ls -l $PGDATA/
    sudo ls -l $PGDATA/pg_wal/
    sudo cat $PGDATA/postgresql.auto.conf

    echo
    echo Redémarrage
    echo

    sudo rm "$PGLOG"
    sudo pg_ctlcluster $V $CLN start

    echo
    echo Attente fin recovery
    echo

    # 2020-05-12 18:01:48 CEST [1919]: [1-1] user=,db=,app=,client= LOG:  starting PostgreSQL 
    DATE_DEBUT=$(head -n1 $PGLOG|awk '{print $1 " " $2}')

    DATE_FIN="X"
    while [ "$DATE_FIN" = "X" ] ; do
        echo "Attente fin du recovery..."
        sleep 4
        DATE_FIN=$(grep "archive recovery complete" $PGLOG|tail -1 |awk '{print $1 " " $2}')
        DATE_FIN=${DATE_FIN:-X}
        tail -n3 $PGLOG
        [ "$DATE_FIN" = "X" ] && echo "Attente..."
    done

    DUREE=$(echo $(psql -Xtc "SELECT extract (epoch from ( '$DATE_FIN'::timestamptz  - '$DATE_DEBUT'::timestamptz ))  " ) ) 

    NB_ARCHIVES_RESTAUREES=$(sudo grep "restored log file" $PGLOG|wc -l)

    log_csv

    echo 
    echo Fin test $t
    echo
    echo  "Résultat transitoire (test $NUM_TEST / $NB_TOTAL_TESTS ) "
    cat $RES_CSV
    if [ $run -gt 1 ] ; then 
        echo
        echo Moyennage
        echo
	# FIXME marche evidemment pas si recvoery encore en cours...
        sed -e "s/PARAM_SOURCE.csv/$RES_CSV/g" moyenne_temps.sql  | psql -X -v sortie="$RES_CSV_MOY" 
        cat $RES_CSV_MOY
    fi

    sudo pg_dropcluster $V $CLN --stop
    sudo -iu postgres $PITRERY_PATH/pitrery -f "$PITRERY_CONF" purge

done  # $t
done  # $run

echo
echo Finish
echo

cat $RES_CSV_MOY

